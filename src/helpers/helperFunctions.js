import {openPopup} from "../redux/ducks/popupDuck";
import {POPUP_NAMES} from "../constants/names";
import {dispatch} from "../redux/store";
import {sendRequest} from "./customRequest";

export const deleteItem = (id, path) => {
    dispatch(openPopup({type: POPUP_NAMES.REMOVE, payload: {id, path}}))
}

export const openEditPopup = (popupType, path, item) => {
    sendRequest(`${path}/${item.id}`)
        .then(res => {
            if (res.id) {
                dispatch(openPopup({ type: popupType, payload: res }));
            } else {
                console.log("Selected item is no longer exist");
            }
        })
}
