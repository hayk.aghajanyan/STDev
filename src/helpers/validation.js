import {MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH} from "../constants/validations";

export const validation = (values) => {
    const errors = {}
    validateEmailAndPassword(values.email, values.password, errors)
    if(values.age > 100) {
        errors.age = 'You look too young for your age :D'
    }

    if(values.password !== values.confirmPassword) {
        errors.confirmPassword = 'passwords are not the same'
    }

    return errors
}


function validateEmailAndPassword(email, password, errors) {
    let emailRegExp = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
    let passwordRegExp = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$/

    if (!email) {
        errors.email = "Required";
    } else if (!emailRegExp.test(email)) {
        errors.email = "Wrong email";
    }

    if (!password) {
        errors.password = "Required";
    } else if (password.length < MIN_PASSWORD_LENGTH) {
        errors.password = `Short password, must be ${MIN_PASSWORD_LENGTH} or more`;
    } else if (password.length > MAX_PASSWORD_LENGTH) {
        errors.password = `Too long password, must be ${MAX_PASSWORD_LENGTH} or less`;
    } else if(passwordRegExp.test(password)) {
        errors.password = 'Password should has at least 1 number, 1 special character and 1 capital letter!'
    }
}
