export function sendRequest(path, method, body, queryString = "", contentType = "application/json") {
    const url = `http://localhost:3000/${path}${queryString}`;
    const headers = {
        "Content-Type": `${contentType}`,
    };

    const options = {
        method,
        headers,
        body: body
            ? JSON.stringify({...body})
            : null,
    };

    return fetch(url, options).then((data) => data.json());
}
