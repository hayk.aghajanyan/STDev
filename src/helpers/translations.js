import {getStoreState} from "../redux/store";

export const t = (key) => {
    const { appDataReducer } = getStoreState();
    const { languagesData, currentLanguage } = appDataReducer;
    return (languagesData && languagesData[currentLanguage] && languagesData[currentLanguage][key]) || key;
};
