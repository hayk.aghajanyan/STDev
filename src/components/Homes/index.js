import {t} from "../../helpers/translations";
import {useDispatch, useSelector} from "react-redux";
import {PATHS, POPUP_NAMES} from "../../constants/names";
import {deleteItem, openEditPopup} from "../../helpers/helperFunctions";
import {openPopup} from "../../redux/ducks/popupDuck";

export const Homes = () => {
    const dispatch = useDispatch()
    const {homes} = useSelector(({homesReducer}) => homesReducer)

    const openAddHomePopup = () => {
        dispatch(openPopup({type: POPUP_NAMES.ADD_HOME}))
    }

    if(!homes) {
        return <div>Loading...</div>
    }

    return (
        <>
            <h3>Homes component</h3>
            <button onClick={openAddHomePopup}>Add home</button>
            <table>
                <thead>
                <tr>
                    <th>{t('User ID')}</th>
                    <th>{t('Title')}</th>
                    <th>{t('Location')}</th>
                    <th>{t('Land sqm')}</th>
                    <th>{t('Place sqm')}</th>
                    <th>{t('No. of bedrooms')}</th>
                    <th>{t('Actions')}</th>
                </tr>
                </thead>
                <tbody>
                {homes.map(item => {
                    return (
                        <tr>
                            <td>{item.id}</td>
                            <td>{item.title}</td>
                            <td>{item.location}</td>
                            <td>{item.landSqm}</td>
                            <td>{item.placeSqm}</td>
                            <td>{item.numbOfBedrooms}</td>
                            <td>
                                <span onClick={() => deleteItem(item.id, PATHS.HOMES)}>{t('Delete')}</span>
                                <span onClick={() => openEditPopup(POPUP_NAMES.ADD_HOME, PATHS.HOMES, item)}>{t('Edit')}</span>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </>
    )
}
