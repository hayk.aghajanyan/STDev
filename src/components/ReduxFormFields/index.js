import {useEffect, useRef, useState} from "react";

export const Input = ({placeholder, meta: {touched, error}, input}) => {
    return (
        <>
            <div className="input-wrapper">
                <input className="form-control" placeholder={placeholder} {...input} />
                {touched && error && (
                    <button style={{color: 'red'}}>
                        {error}
                    </button>
                )}
            </div>
        </>
    );
};

export const Dropdown = ({children, placeholder, input, dropdownBtnContent}) => {
    const [isToggled, setIsToggled] = useState(false)
    const ddRef = useRef(null)

    const handleDropdownToggle = (e) => {
        // if (!loginRef.current.contains(e.target)) {
        if (!e.composedPath().includes(ddRef.current)) {
            setIsToggled(false);
        }
    };

    useEffect(() => {
        document.body.addEventListener("click", handleDropdownToggle);
        return () => document.body.removeEventListener("click", handleDropdownToggle);
    }, []);

    const toggleHandler = (e) => {
        e.stopPropagation();
        setIsToggled((isToggled) => !isToggled);
    };

    const closeHandler = () => {
        setIsToggled(false);
    };

    return (
        <>
            <div
                ref={ddRef}
                className={isToggled ? 'opened' : ''}
            >
                {placeholder && <span>{placeholder}</span>}
                <button type="button" className="dropdown-btn" onClick={toggleHandler}>
                    {dropdownBtnContent}
                </button>
                <div onClick={closeHandler} className="dropdown-inner" {...input}>
                    {children}
                </div>
            </div>
        </>
    );
}
