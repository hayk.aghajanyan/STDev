import {t} from "../../helpers/translations";
import {useDispatch, useSelector} from "react-redux";
import {PATHS, POPUP_NAMES} from "../../constants/names";
import {deleteItem, openEditPopup} from "../../helpers/helperFunctions";
import {openPopup} from "../../redux/ducks/popupDuck";

export const Users = () => {
    const dispatch = useDispatch()
    const {users} = useSelector(({usersReducer}) => usersReducer)

    const openAddUserPopup = () => {
        dispatch(openPopup({type: POPUP_NAMES.ADD_USER}))
    }

    if(!users) {
        return <div>Loading...</div>
    }

    return (
        <>
            <h3>Users component</h3>
            <button onClick={openAddUserPopup}>Add user</button>
            <table>
                <thead>
                    <tr>
                        <th>{t('First Name')}</th>
                        <th>{t('Last Name')}</th>
                        <th>{t('Email')}</th>
                        <th>{t('Actions')}</th>
                    </tr>
                </thead>
                <tbody>
                {users.map(item => {
                    return (
                        <tr key={item.id}>
                            <td>{item.firstName}</td>
                            <td>{item.lastName}</td>
                            <td>{item.email}</td>
                            <td>
                                <span onClick={() => deleteItem(item.id, PATHS.USERS)}>{t('Delete')}</span>
                                <span onClick={() => openEditPopup(POPUP_NAMES.ADD_USER, PATHS.USERS, item)}>{t('Edit')}</span>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </>
    )
}
