import {useDispatch, useSelector} from "react-redux";
import {sendRequest} from "../../../helpers/customRequest";
import {closePopup} from "../../../redux/ducks/popupDuck";
import {PATHS} from "../../../constants/names";
import {deleteUser} from "../../../redux/ducks/usersDuck";
import {deleteHome} from "../../../redux/ducks/homesDuck";

export const Remove = () => {
    const dispatch = useDispatch();
    const {id, path} = useSelector(({ popupReducer }) => popupReducer.payload);

    const deleteItem = () => {
        sendRequest(`${path}/${id}`, "DELETE")
            .then(() => {
                console.log('Successfully deleted')
                switch (path) {
                    case PATHS.USERS:
                        dispatch(deleteUser(id))
                        break
                    case PATHS.HOMES:
                        dispatch(deleteHome(id))
                        break
                    default:
                        break
                }
            })
            .catch(e => console.log('Something went wrong during deleting item', e))
        close();
    };

    const close = () => {
        dispatch(closePopup());
    };

    return (
        <>
            <p>Are You sure to delete this item?</p>
            <div>
                <button onClick={deleteItem} type="submit">
                    Yes
                </button>
                <button onClick={close} type="button">
                    Cancel
                </button>
            </div>
        </>
    );
}
