import {Field, formValueSelector, reduxForm, initialize} from "redux-form";
import {t} from "../../../helpers/translations";
import {Input} from "../../ReduxFormFields";
import {closePopup} from "../../../redux/ducks/popupDuck";
import {connect, useDispatch} from "react-redux";
import {sendRequest} from "../../../helpers/customRequest";
import {PATHS} from "../../../constants/names";
import {addUser, editUser} from "../../../redux/ducks/usersDuck";
import {validation as inputValidations} from "../../../helpers/validation";
import {getStoreState} from "../../../redux/store";

function NewUser({handleSubmit, pristine, submitting, itemData}) {
    const dispatch = useDispatch()

    if(itemData) {
        const {
            firstName,
            lastName,
            email,
            age,
        } = itemData

        dispatch(initialize('userRegister', {
            firstName,
            lastName,
            email,
            age,
        }, true))
    }

    const close = () => {
        dispatch(closePopup())
    };

    const customSubmit = ({firstName, lastName, email, age, password}, dispatch) => {
        if(itemData) {
            const newUser = {
                firstName,
                lastName,
                email,
                age,
                password,
            }
            sendRequest(`${PATHS.USERS}/${itemData.id}`, "PUT", newUser)
                .then(() => {
                    dispatch(editUser({id: itemData.id, newUser}))
                })
                .finally(() => dispatch(closePopup()))

        } else {
            const id = (getStoreState().usersReducer.users.length + 1).toString()
            const newUser = {
                id,
                firstName,
                lastName,
                email,
                age,
                password,
            }
            sendRequest(PATHS.USERS, "POST", newUser)
                .then(() => {
                    dispatch(addUser(newUser))
                })
                .finally(() => dispatch(closePopup()))
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit(customSubmit)}>
                <Field placeholder={t("First name")} name="firstName" component={Input} type="text"/>
                <Field placeholder={t("Last name")} name="lastName" component={Input} type="text"/>
                <Field placeholder={t("Email")} name="email" component={Input} type="text"/>
                <Field placeholder={t("Age")} name="age" component={Input} type="text"/>
                <Field placeholder={t("Password")} name="password" component={Input} type="password"/>
                <Field placeholder={t("Confirm password")} name="confirmPassword" component={Input} type="password"/>

                <button
                    disabled={pristine || submitting}
                    type="submit"
                >
                    {itemData ? t('Edit') : t("Add")}
                </button>
                <button onClick={close}>{t('Cancel')}</button>
            </form>
        </div>
    )
}

function mapStateToProps(state) {
    const selector = formValueSelector("userRegister")
    const values = selector(state, 'firstName')
    if(state.popupReducer) {
        return {
            values,
            itemData: state.popupReducer.payload,
        };
    }

    return {values}
}

export default reduxForm({
    form: "userRegister",
    validate: inputValidations,
})(connect(mapStateToProps)(NewUser));
