import {useSelector} from "react-redux";
import {POPUP_NAMES} from "../../constants/names";
import NewHome from './NewHome'
import NewUser from './NewUser'
import {Remove} from "./RemovePopup";

export const Popup = () => {
    const popupData = useSelector(({popupReducer}) => popupReducer)

    if (!popupData || !popupData.type) {
        return null
    }
    let popup = null
    let title = null
    console.log('popupData', popupData)

    switch (popupData.type) {
        case POPUP_NAMES.ADD_USER:
            popup = <NewUser/>
            title = POPUP_NAMES.ADD_USER
            break
        case POPUP_NAMES.ADD_HOME:
            popup = <NewHome/>
            title = POPUP_NAMES.ADD_HOME
            break
        case POPUP_NAMES.REMOVE:
            popup = <Remove/>
            title = POPUP_NAMES.REMOVE
            break
        default:
            throw new Error('Wrong popupData type')
    }


    return (
        <div className="popup-overlay">
            <div className='popup'>
                <div className="popup-header">
                    {title && <h2 className="popup-title colorRed">{title}</h2>}
                </div>
                <div className="popup-content">
                    {popup}
                </div>
            </div>
        </div>
    );
}





