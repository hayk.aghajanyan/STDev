import {Field, formValueSelector, reduxForm, initialize} from "redux-form";
import {t} from "../../../helpers/translations";
import {Dropdown, Input} from "../../ReduxFormFields";
import {closePopup} from "../../../redux/ducks/popupDuck";
import {connect, useDispatch} from "react-redux";
import {sendRequest} from "../../../helpers/customRequest";
import {PATHS} from "../../../constants/names";
import {getStoreState} from "../../../redux/store";
import {addHome, editHome} from "../../../redux/ducks/homesDuck";

function NewHome({handleSubmit, pristine, submitting, itemData}) {
    const dispatch = useDispatch()

    if(itemData) {
        const {
            title,
            location,
            landSqm,
            placeSqm,
            numbOfBedrooms,
        } = itemData

        dispatch(initialize('homeRegister', {
            title,
            location,
            landSqm,
            placeSqm,
            numbOfBedrooms,
        }, true))
    }

    const close = () => {
        dispatch(closePopup())
    };

    const customSubmit = ({title, location, landSqm, placeSqm, numbOfBedrooms}, dispatch) => {
        if(itemData) {
            const newHome = {
                title,
                location,
                landSqm,
                placeSqm,
                numbOfBedrooms,
            }
            sendRequest(`${PATHS.HOMES}/${itemData.id}`, "PUT", newHome)
                .then(() => {
                    dispatch(editHome({id: itemData.id, newHome}))
                })
                .finally(() => dispatch(closePopup()))

        } else {
            const id = (getStoreState().homesReducer.homes.length + 1).toString()
            const newHome = {
                id,
                title,
                location,
                landSqm,
                placeSqm,
                numbOfBedrooms,
            }
            sendRequest(PATHS.HOMES, "POST", newHome)
                .then(() => {
                    dispatch(addHome(newHome))
                })
                .finally(() => dispatch(closePopup()))
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit(customSubmit)}>
                <Field placeholder={t("Title")} name="title" component={Input} type="text"/>
                <Field placeholder={t("Location")} name="location" component={Input} type="text"/>
                <Field placeholder={t("Land sqm")} name="landSqm" component={Input} type="text"/>
                <Field placeholder={t("Place sqm")} name="placeSqm" component={Input} type="text"/>
                <Field placeholder={t("No. of bedrooms")} name="numbOfBedrooms" component={Input} type="text"/>
                <Field placeholder={t("User id")} name="userID" component={Dropdown} dropdownBtnContent={'User ID'}/>


                <button
                    disabled={pristine || submitting}
                    type="submit"
                >
                    {itemData ? t('Edit') : t("Add")}
                </button>
                <button onClick={close}>{t('Cancel')}</button>
            </form>
        </div>
    )
}

function mapStateToProps(state) {
    const selector = formValueSelector("homeRegister")
    const values = selector(state, 'Title')
    if(state.popupReducer) {
        return {
            values,
            itemData: state.popupReducer.payload,
        };
    }

    return {values}
}

export default reduxForm({
    form: "homeRegister",
})(connect(mapStateToProps)(NewHome));
