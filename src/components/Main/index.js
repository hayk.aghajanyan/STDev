import {Route, Switch} from "react-router-dom";
import {Users} from "../Users";
import {Homes} from "../Homes";

export const Main = () => (
    <Switch>
        <Route exact path={['/users', '/']} component={Users}/>
        <Route path={'/homes'} component={Homes}/>
    </Switch>
)

