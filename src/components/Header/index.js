import {NavLink} from "react-router-dom";

export const Header = () => {
    return (
        <header>
            <NavLink to={'/'} exact>To Users</NavLink>
            <NavLink to={'homes'}>To Homes</NavLink>
        </header>
    )
}
