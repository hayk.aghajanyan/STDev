import './App.css';
import {useEffect} from "react";
import {Header} from "./components/Header";
import {Main} from "./components/Main";
import {Popup} from "./components/Popup";
import {useDispatch} from "react-redux";
import {sendRequest} from "./helpers/customRequest";
import {setUsers} from "./redux/ducks/usersDuck";
import {setLoader} from "./redux/ducks/appDataDuck";
import {batchActions} from "redux-batched-actions";
import {setHomes} from "./redux/ducks/homesDuck";

function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setLoader(true))
        Promise.all([sendRequest('users'), sendRequest('homes')])
            .then(res => {
                dispatch(batchActions([setUsers(res[0]), setHomes(res[1])]))
            })
            .catch(err => console.log('failed request: App.js 18', err))
            .finally(() => dispatch(setLoader(false)))
    }, [])


    return (
        <div className="App">
            <Header/>
            <Main/>
            <Popup/>
        </div>
    );
}

export default App;
