import {createAction, createReducer} from "../../helpers/reduxHelper";

const SET_USERS = 'SET_USERS'
const DELETE_USER = 'DELETE_USER'
const ADD_USER = 'ADD_USER'
const EDIT_USER = 'EDIT_USER'

export const setUsers = createAction(SET_USERS)
export const deleteUser = createAction(DELETE_USER)
export const addUser = createAction(ADD_USER)
export const editUser = createAction(EDIT_USER)

const initialState = {
    users: []
}

export const usersReducer = createReducer(initialState, (state, {value}) => ({
    [SET_USERS]: () => ({
        ...state,
        users: value
    }),
    [DELETE_USER]: () => {
        const remainedUsers = state.users.filter(item => item.id !== value)
        return {
            ...state,
            users: remainedUsers
        }
    },
    [ADD_USER]: () => {
        state.users.unshift(value)
        return {
            ...state
        }
    },
    [EDIT_USER]: () => {
        const currentUser = state.users.find(user => user.id === value.id)
        Object.assign(currentUser, value.newUser)
        return {
            ...state
        }
    }
}))
