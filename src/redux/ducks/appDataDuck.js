import {createAction, createReducer} from "../../helpers/reduxHelper";

const SET_LANGUAGES = 'SET_LANGUAGE'
const CHOOSE_LANGUAGE = 'CHOOSE_LANGUAGE'
const SET_LOADER = 'SET_LOADER'

export const setLanguages = createAction(SET_LANGUAGES)
export const chooseLanguages = createAction(CHOOSE_LANGUAGE)
export const setLoader = createAction(SET_LOADER)

const initialState = {
    currentLanguage: 'en',
    languagesData: {
        'en': {}
    },
    loader: false
}

export const appDataReducer = createReducer(initialState, (state, {value}) => ({
    [SET_LANGUAGES]: () => ({
        ...state,
        languagesData: value
    }),
    [CHOOSE_LANGUAGE]: () => ({
        ...state,
        currentLanguage: value
    }),
    [SET_LOADER]: () => ({
        ...state,
        loader: value
    })
}))
