import {createAction, createReducer} from "../../helpers/reduxHelper";

const SET_HOMES = 'SET_HOMES'
const DELETE_HOME = 'DELETE_HOME'
const ADD_HOME = 'ADD_HOME'
const EDIT_HOME = 'EDIT_HOME'

export const setHomes = createAction(SET_HOMES)
export const deleteHome = createAction(DELETE_HOME)
export const addHome = createAction(ADD_HOME)
export const editHome = createAction(EDIT_HOME)

const initialState = {
    homes: []
}

export const homesReducer = createReducer(initialState, (state, {value}) => ({
    [SET_HOMES]: () => ({
        ...state,
        homes: value
    }),
    [DELETE_HOME]: () => {
        const remainedHomes = state.homes.filter(item => item.id !== value)
        return {
            ...state,
            homes: remainedHomes
        }
    },
    [ADD_HOME]: () => {
        state.homes.unshift(value)
        return {
            ...state
        }
    },
    [EDIT_HOME]: () => {
        const currentHome = state.homes.find(home => home.id === value.id)
        Object.assign(currentHome, value.newHome)
        return {
            ...state
        }
    }
}))
