import {createAction, createReducer} from "../../helpers/reduxHelper";

const OPEN_POPUP = 'OPEN_POPUP'
const CLOSE_POPUP = 'CLOSE_POPUP'
const SET_POPUP_LOADER = 'SET_POPUP_LOADER'

export const openPopup = createAction(OPEN_POPUP)
export const closePopup = createAction(CLOSE_POPUP)
export const setPopupLoader = createAction(SET_POPUP_LOADER)


export const popupReducer = createReducer(null, (state, {value}) => ({
    [OPEN_POPUP]: () => value,
    [CLOSE_POPUP]: () => null,
}))
