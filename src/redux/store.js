import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import { enableBatching } from 'redux-batched-actions';
import { reducers} from "./index";

const compose = composeWithDevTools

const store = createStore(
    enableBatching(combineReducers(reducers)),
    compose(applyMiddleware(thunkMiddleware)),
);

window.getState = store.getState;

export const getStoreState = store.getState;
export const { dispatch } = store;
export default store;
