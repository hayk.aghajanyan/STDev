import { reducer as formReducer } from "redux-form";
import { usersReducer } from "./ducks/usersDuck";
import {popupReducer} from "./ducks/popupDuck";
import {appDataReducer} from "./ducks/appDataDuck";
import {homesReducer} from "./ducks/homesDuck";

export const reducers = {
    form: formReducer,
    usersReducer,
    popupReducer,
    appDataReducer,
    homesReducer,
}
