export const POPUP_NAMES = {
    ADD_USER: 'add user',
    EDIT_USER: 'edit user',
    ADD_HOME: 'add home',
    EDIT_HOME: 'edit home',
    REMOVE: 'remove',
}

export const PATHS = {
    USERS: 'users',
    HOMES: 'homes',
}
